package util;

public class StringUtils {

    public static String[] toStringsArray(Object... arguments) {
        String[] str = new String[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            str[i] = arguments.toString();
        }
        return str;
    }

    public static boolean isItSentence(String str) {
        return str.matches("(?:[.!? ]|^)([A-Z][^.!?\n]*[.!?])(?= |[A-Z]|$)");
    }

    public static String[] split(String text, String word) {
        return text.split(word);
    }

    public static String replaceVowels(String text) {
        return text.replace("[aeiouyAEIOUY]", "_");
    }
}
