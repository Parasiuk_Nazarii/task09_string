package view;

import controller.BigTaskController;
import controller.FileController;
import controller.TextController;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private Locale locale;
    private ResourceBundle bundle;
    private TextController tc = new TextController();

    public void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();


        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void pressButton2() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void pressButton3() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void pressButton4() throws IOException {
        File file = new FileController().chooseFile();
        tc = new TextController(FileController.fileToText(file));
        System.out.println(tc.getText().toString() + "************");
    }

    private void pressButton5() throws IOException {
        if (tc == null) {
            System.out.println("You need choose a file before");
        } else {
            new BigTaskController(tc).run();
        }

    }

    private void pressButton6() throws IOException {
        Runtime.getRuntime().exec("cmd /c notepad.exe  src/main/resources/TXT/out.txt");

    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
