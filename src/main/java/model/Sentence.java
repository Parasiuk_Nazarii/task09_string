package model;

public class Sentence implements Comparable<Sentence> {
    private String it;

    public Sentence(String it) {
        this.it = it;
    }

    public Sentence() {

    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = new Sentence(it).getIt();
    }

    public Word[] toWordsArray() {
        String[] arr = it.split(" ");
        Word[] words = new Word[arr.length];
        for (int i = 0; i < words.length; i++) {
            words[i] = new Word(arr[i]);
        }
        return words;
    }

    public boolean isWordPresent(Word input) {
        Word[] words = toWordsArray();
        for (Word word : words) {
            if (word.equals(input)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sentence)) return false;

        Sentence sentence = (Sentence) o;

        return getIt() != null ? getIt().equals(sentence.getIt()) : sentence.getIt() == null;
    }

    @Override
    public int hashCode() {
        return getIt() != null ? getIt().hashCode() : 0;
    }

    @Override
    public int compareTo(Sentence o) {
        return this.toWordsArray().length - o.toWordsArray().length;
    }

    @Override
    public String toString() {
        return it;
    }
}
