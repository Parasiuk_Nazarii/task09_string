package model;

public class Word {
    private String it;
    private int length;
    private String vowels = "[aeiouyAEIOUY]";

    public Word(String it) {
        this.it = it.replaceAll("[\\W]", "");
        this.length = this.it.length();
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it.replaceAll("[^a-zA-Z]", "");
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;

        Word word = (Word) o;

        return getIt() != null ?
                getIt().equalsIgnoreCase(word.getIt())
                : word.getIt() == null;
    }

    @Override
    public String toString() {
        return it;
    }

    @Override
    public int hashCode() {
        return getIt() != null ? getIt().hashCode() : 0;
    }


    public static boolean isItWord(String str) {
        return (str.length() == str.replace("[^a-zA-Z]", "").length());
    }

    public int compareTo(Word w) {
        return this.getIt().compareTo(w.getIt());
    }

    public int compareToByVowelPercent(Word w) {
        double thisConsonant = this.getIt().replaceAll(vowels, "").length();
        double thisLength = this.getIt().length();
        double thisPercent = thisConsonant / thisLength;

        double thatConsonant = w.getIt().replaceAll(vowels, "").length();
        double thatLength = w.getIt().length();
        double thatPercent = thatConsonant / thatLength;
        return (int) ((thatPercent - thisPercent) * 100);
    }

    public int compareByFirstConsonant(Word w) {
        String thisFirst = this.getIt().replaceAll(vowels, "");
        String otherFirst = w.getIt().replaceAll(vowels, "");
        return thisFirst.compareTo(otherFirst);
    }

    public int compareByLetterCount(Word w, String c) {
        int thisCount = this.getIt().split(c, -1).length - 1;
        int otherCount = w.getIt().split(c, -1).length - 1;
        int compar = thisCount - otherCount;
        if (thisCount == otherCount) {
            return this.compareTo(w);
        } else
            return compar;
    }
}
