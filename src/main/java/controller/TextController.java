package controller;

import model.Sentence;
import model.Text;
import model.Word;

import java.util.*;
import java.util.stream.Collectors;

public class TextController implements BigTaskInterface {
    private Text text;
    private Sentence[] sentences;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Sentence[] getSentences() {
        return sentences;
    }

    public void setSentences(Sentence[] sentences) {
        this.sentences = sentences;
    }

    public TextController(Text text) {
        this.text = text;
        this.sentences = text.toSentences();
    }

    public TextController() {
    }

    public int countOfSentencesWithDublicats() {
        return 0;
    }

    public void printSentencesInOrder() {
        Sentence[] ordered = new Sentence[sentences.length];
        for (int i = 0; i < ordered.length; i++) {
            ordered[i] = sentences[i];
        }
        Arrays.sort(ordered, Sentence::compareTo);
        for (Sentence sentence : ordered) {
            System.out.println(sentence);
        }
    }

    public String exclusiveWords() {
        String exclusiveWords = "";
        Word[] words = sentences[0].toWordsArray();
        boolean isExclusive;
        wordsLoop:
        for (Word word : words) {
            isExclusive = true;
            for (int i = 1; i < sentences.length; i++) {
                if (sentences[i].isWordPresent(word)) {
                    isExclusive = false;
                    continue wordsLoop;
                }
            }
            if (isExclusive) {
                exclusiveWords += word + "; ";
            }
        }
        return exclusiveWords;
    }

    public void findAndPrint(int length) {
        for (Sentence sentence : sentences) {
            String it = sentence.getIt();
            if (it.matches("[\\w\\,\\s]*\\?")) {
                Word[] words = sentence.toWordsArray();
                for (Word word : words) {
                    if (word.getLength() == length) {
                        System.out.println(word);
                    }
                }
            }
        }
    }

    public Text switchWords() {
        String text = "";
        for (Sentence sentence : sentences) {
            Word[] words = sentence.toWordsArray();
            int firstIndex = -1;
            int maxIndex = 0;
            for (int i = 0; i < words.length; i++) {
                if (words[i].getIt().matches("^[aeiouAEIOU]\\w+")) {
                    firstIndex = i;
                    break;
                }
            }
            for (int i = 0; i < words.length; i++) {
                if (words[i].getLength() > words[maxIndex].getLength()) {
                    maxIndex = i;
                }
            }
            if (firstIndex != -1) {
                Word buffer = words[firstIndex];
                words[firstIndex] = words[maxIndex];
                words[maxIndex] = buffer;
            }
            for (Word word : words) {
                text += word.getIt() + " ";
            }
            text = text.substring(0, text.length() - 1);
            text += sentence.getIt().substring(sentence.getIt().length() - 1);
        }
        return new Text(text);
    }

    public void printWords() {
        Word[] words = text.toWordsArray();
        Arrays.sort(words, Word::compareTo);
        for (int i = 0; i < words.length; i++) {
            if (i > 0 && !words[i].getIt().substring(0, 1)
                    .equalsIgnoreCase(words[i - 1].getIt().substring(0, 1))) {
                System.out.println();
            }
            System.out.println(words[i]);

        }
    }

    public Text sortByVowelPercent() {
        String newText = "";
        Word[] words = text.toWordsArray();
        Arrays.sort(words, Word::compareToByVowelPercent);
        for (int i = 0; i < words.length; i++) {
            newText += words[i].getIt() + " ";
        }
        return new Text(newText);
    }

    public Text sortByFirstConsonant() {
        String newText = "";
        List<Word> list = new ArrayList<>();
        Word[] words = text.toWordsArray();
        for (Word word : words) {
            if (word.getIt().matches("^[aeiouAEIOU]\\w+")) {
                list.add(word);
            }
        }
        List<Word> sortedWords = list.stream()
                .sorted(Word::compareByFirstConsonant)
                .collect(Collectors.toList());
        for (Word word : sortedWords) {
            newText += word.getIt() + " ";
        }

        return new Text(newText);
    }

    public Text sortByLetterCount(String c) {
        String newText = "";
        Word[] words = text.toWordsArray();
        System.out.println(words);
        Arrays.sort(words, (w1, w2) -> w1.compareByLetterCount(w2, c));
        for (Word word : words) {
            newText += word.getIt() + " ";
        }
        return new Text(newText);
    }

    @Override
    public Map<Word, Integer> sortByCount(List<Word> list) {
        Map<Word, Integer> map = new HashMap<>();
        for (Word word : list) {
            map.put(word, text.enterCount(word));
        }
        Map<Word, Integer> sortedMap = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return sortedMap;
    }

    //--------------------------10----------------------
    public Text deleteBetween(char a, char b) {
        return null;
    }

    public Text deleteConsonants(int length) {
        return null;
    }

    public Text sortByLetterCountDesc(char c) {
        return null;
    }

    public String maxPolindrom() {
        return null;
    }

    public Text deleteNextFirstLetter() {
        return null;
    }

    public Text deletePreviousLastLetter() {
        return null;
    }

    public Text replaceInSentence(int number, String sub) {
        return null;
    }
}
