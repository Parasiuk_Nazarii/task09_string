package controller;

import model.Sentence;
import model.Word;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BigTaskController {
    private TextController tc = new TextController();

    public BigTaskController(TextController tc) {
        this.tc = tc;
    }

    public void run() throws IOException {
        FileWriter fw = new FileWriter("src/main/resources/TXT/out.txt");
        Sentence[] sentences = tc.getSentences();

        for (int i = 0; i < sentences.length; i++) {
            System.out.println(i + "->" + sentences[i].getIt());
            fw.write(i + "->" + sentences[i].getIt() + "\n");
        }

        tc.printSentencesInOrder();

        System.out.println(tc.exclusiveWords());
        fw.write("Exclusive words: " + tc.exclusiveWords() + "\n");

        tc.findAndPrint(5);

        String x = tc.sortByVowelPercent().toString();
        System.out.println(x);
        fw.write(x + "\n");

        String x1 = tc.switchWords().toString();
        System.out.println(x1);
        fw.write(x1 + "\n");

        tc.printWords();

        String x2 = tc.sortByFirstConsonant().toString();
        System.out.println(x2);
        fw.write(x2 + "\n");

        String x3 = tc.sortByLetterCount("i").toString();
        System.out.println(x3);
        fw.write(x3 + "\n");

        List<Word> words = new ArrayList<>();
        words.add(new Word("is"));
        words.add(new Word("this"));
        Map<Word, Integer> x4 = tc.sortByCount(words);
        System.out.println(x4);
        fw.write(x4.toString() + "\n");
        fw.close();
    }
}
